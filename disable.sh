if [ "$OLD_PATH" != "" ]
then
	export PATH=$OLD_PATH
	export LD_LIBRARY_PATH=$OLD_LD_LIBRARY_PATH
	export LD_PRELOAD=$OLD_LD_PRELOAD
	echo "\$PATH back to: $PATH"
	echo "\$LD_LIBRARY_PATH back to: $LD_LIBRARY_PATH"
	echo "\$LD_PRELOAD back to: $LD_PRELOAD"
fi

