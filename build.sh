#!/bin/bash

ROOT=`realpath $(dirname "$0")`
CPU_CORES=`grep -c "^processor" /proc/cpuinfo`
BUILD_DIR=build_dir

cd "$ROOT"

if test "$1" == "FORCE"
then
	rm -r "$BUILD_DIR"
fi

export LD_LIBRARY_PATH="$ETA_PATH/lib"

if test -e "$BUILD_DIR"
then
	cd "$BUILD_DIR"
	make -j$CPU_CORES -s && make install
else
	export PREFIX="$ROOT/install"
	mkdir "$BUILD_DIR"
	cd "$BUILD_DIR"
	export CXXFLAGS="-w -O3 -I$ETA_PATH/include/"
	export CPPFLAGS=$CXXFLAGS
	export CFLAGS="-O3 -I$ETA_PATH/include/"
	export LDFLAGS="-L$ETA_PATH/lib -leta_oracle"
	../configure --prefix="$PREFIX" --disable-multilib
        make -s -j$CPU_CORES && make install
fi

