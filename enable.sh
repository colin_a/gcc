INSTALL_DIR=`realpath $(dirname $BASH_SOURCE)`/install

export GOMP_CPU_AFFINITY=`hwloc-calc --physical-output --intersect PU --no-smt all`

if [ "$OLD_PATH" == "" ]
then
	OLD_PATH=$PATH
	OLD_LD_LIBRARY_PATH=$LD_LIBRARY_PATH
	OLD_LD_PRELOAD=$LD_PRELOAD
fi

bin_path=$INSTALL_DIR/bin
lib_path=$INSTALL_DIR/lib:$INSTALL_DIR/lib64
lib=$ETA_PATH/lib/x86_64-linux-gnu/libeta_oracle.so

export PATH=$bin_path:$OLD_PATH
export LD_LIBRARY_PATH=$lib_path:$ETA_PATH/lib:$ETA_PATH/lib/x86_64-linux-gnu/:$OLD_LD_LIBRARY_PATH

echo "New \$PATH: $PATH"
echo "New \$LD_LIBRARY_PATH: $LD_LIBRARY_PATH"
echo "New \$LD_PRELOAD: $LD_PRELOAD"
